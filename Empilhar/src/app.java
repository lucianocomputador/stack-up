import java.util.ArrayList;

/**
 * Problema de empilhamento de caixas em Java
 * 
 * @local IFPB - Monteiro
 * @disciplina Análise de Algoritimos
 * @author Luciano Azevedo
 * @data 10/05/2019
 * 
 *       Programação dinâmica
 */

public class app {

	/* Testando */
	public static void main(String[] args) {
		/**
		 * Chamar os testes
		 */
		testesBox testes = new testesBox();

		/**
		 * Chamar o empilhamento
		 */
		stack stack_up = new stack();

		ArrayList<caixa> arr = testes.teste16();

		// Mostrar caixa a serem empilhadas
		System.out.println("Caixas entrada:");
		for (caixa caixa : arr) {
			System.out.println("\t" + caixa);
		}

		// Motrar as caixas na ordem a serem empilhadas
		stack_up.stack_up(arr, arr.size());
	}
}
