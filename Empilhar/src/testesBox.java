import java.util.ArrayList;

/**
 * Problema de empilhamento de caixas em Java
 * 
 * @local IFPB - Monteiro
 * @disciplina Análise de Algoritimos
 * @author Luciano Azevedo
 * @data junho/2019
 * 
 *       Classe responsável por criar os testes
 */

public class testesBox {
	caixa box = new caixa();

	/*
	 * Criando as caixas Box(altura, largura, comprimento) id, altura, largura,
	 * profundidade
	 */
	/**
	 * Teste com 1 caixa de valores iguais
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste00() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 13)); // caixa 1
		return arr;
	}

	/**
	 * Teste com 1 caixa com valore diferentes
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste01() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 12, 15, 17)); // caixa 1
		return arr;
	}

	/**
	 * Teste com 1 caixa com altura maior
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste02() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 17, 15, 12)); // caixa 1
		return arr;
	}

	/**
	 * Teste com 1 caixa com largura maior
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste03() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 15, 17, 12)); // caixa 1
		return arr;
	}

	/**
	 * Teste com 1 caixa com comprimento maior
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste04() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 15, 12, 17)); // caixa 1
		return arr;
	}

	/**
	 * Teste com 2 caixa de valores iguais
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste05() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 13)); // caixa 1
		arr.add(new caixa(2, 13, 13, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com comprimento da segunda diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste06() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 13)); // caixa 1
		arr.add(new caixa(2, 13, 13, 15)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com comprimento da primeira diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste07() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 15)); // caixa 1
		arr.add(new caixa(2, 13, 13, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com largura da primeira diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste08() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 18, 13)); // caixa 1
		arr.add(new caixa(2, 13, 13, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com largura da segunda diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste09() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 13)); // caixa 1
		arr.add(new caixa(2, 13, 18, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com altura da segunda diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste10() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 13)); // caixa 1
		arr.add(new caixa(2, 17, 13, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com altura da primeira diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste11() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 17, 13, 13)); // caixa 1
		arr.add(new caixa(2, 13, 13, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com altura e largura da primeira diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste12() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 17, 14, 13)); // caixa 1
		arr.add(new caixa(2, 13, 13, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com altura e largura da segunda diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste13() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 13)); // caixa 1
		arr.add(new caixa(2, 17, 14, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com altura e comprimento da primeira diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste14() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 17, 13, 19)); // caixa 1
		arr.add(new caixa(2, 13, 13, 13)); // caixa 2
		return arr;
	}

	/**
	 * Teste com 2 caixa com altura e comprimento da segunda diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste15() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 13, 13, 13)); // caixa 1
		arr.add(new caixa(2, 17, 13, 19)); // caixa 2
		return arr;
	}
	
	/**
	 * Teste com 2 caixa com altura e comprimento da segunda diferente
	 * 
	 * @return
	 */
	public ArrayList<caixa> teste16() {
		ArrayList<caixa> arr = new ArrayList<caixa>();
		arr.add(new caixa(1, 4, 6, 7)); // caixa 1
		arr.add(new caixa(2, 1, 2, 3)); // caixa 2
		arr.add(new caixa(3, 4, 5, 6)); // caixa 3
		arr.add(new caixa(4, 10, 12, 32)); // caixa 4
		return arr;
	}
	
	
}
