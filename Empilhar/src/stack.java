import java.util.ArrayList;
import java.util.Collections;

/**
 * Problema de empilhamento de caixas em Java
 * 
 * @local IFPB - Monteiro
 * @disciplina Análise de Algoritimos
 * @author Luciano Azevedo
 * @data junho/2019
 * 
 *       Classe responsável por reconhecer as possíveis dimensões das caixas a
 *       serem empilhadas
 */

public class stack {

	empilha empilha = new empilha();

	/**
	 * Conhecer as possíves variações das dimensões das caixas, que pode ser formada
	 * com cada tipo de caixas. Depois classifica em ordem decescente as possíveis
	 * combinações das dimensões. Em seguida empilha.
	 */
	public void stack_up(ArrayList<caixa> arr, int n) {

		// Conhecer //

		/** Lista auxiliar para montar 3 x a quantidade de caixas */
		ArrayList<caixa> rot = new ArrayList<caixa>();

		int x;
		for (int i = 0; i < n; i++) {
			caixa box = arr.get(i);

			/**
			 * Caixa Orignal onde largura é a maxima entre largura e produndidade,
			 * profundidade é o minimo entre largura e profundidade
			 */
			rot.add(new caixa(box.id, box.h, Math.max(box.w, box.d), Math.min(box.w, box.d), 1));

			/**
			 * Primeira rotação da caixa onde largura é a maxima entre altura e
			 * profundidade, profundidade é o minimo entre altura e profundidade
			 */
			rot.add(new caixa(box.id, box.w, Math.max(box.h, box.d), Math.min(box.h, box.d), 1));

			/**
			 * Segunda rotação da caixa onde largura é a maxima entre largura e altura,
			 * profundidade é o minimo entre largura e altura
			 */
			rot.add(new caixa(box.id, box.d, Math.max(box.w, box.h), Math.min(box.w, box.h), 1));

		}

		// Classificar //

		/**
		 * Classificando as caixas pela área decrescente.
		 */
		Collections.sort(rot);
		
		// mostra como ficou a ordem da maior área para menor
//		for (caixa caixa : rot) {
//			System.out.println(caixa);
//		}

		// Empilhar //

		/**
		 * Chamada para empilhar as caixas ja com áreas
		 */
		empilha.empilhar(rot, n);

	}

}
