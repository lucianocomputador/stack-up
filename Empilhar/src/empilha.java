import java.util.ArrayList;

/**
 * Problema de empilhamento de caixas em Java
 * 
 * @local IFPB - Monteiro
 * @disciplina Análise de Algoritimos
 * @author Luciano Azevedo
 * @data junho/2019
 * 
 *       Classe responsável pelo empilhamento
 */

public class empilha {

	/**
	 * Empilhar as caixas ordenadas por área, sem repetir as já empilhadas
	 * 
	 * @param rot lista de caixas a serem empilhadas
	 * @n quantidade de caixas na entrada
	 */
	public void empilhar(ArrayList<caixa> rot, int n) {

//		ArrayList<caixa> boxEmpilhar = new ArrayList<caixa>();

		ArrayList<caixa> xbox = new ArrayList<caixa>();
		
		int max = -1;
		/* Escolha o máximo de todos os valores de xbox */
		for (int i = 0; i < rot.size(); i++) {

			max = Math.max(max, rot.get(i).area);
			if (i > 1)
				max = Math.max(max, rot.get(i - 1).area);

			if (empilhado(xbox, rot.get(i))) {
				max = -1;
			}else {
				xbox.add(rot.get(i));
			}
					
		}
		
		System.out.println(xbox.size() + " Caixas empilhadas.");
		for (caixa box : xbox) { // mostrar caixas empilhadas em oredem decrescente
			System.out.println("\t" + box);
		}
		
		

//		for (int i = 0; i < rot.size(); i++) { // pecorrer todas as caixas para encontrar as maiores
//
//			boolean empilhado = false; // para saber se a caixa da vez ja foi empinhada
//
//			for (caixa box : boxEmpilhar) { // verificar nas caixas empilhadas se a atual foi empilhada
//				if (box.id == rot.get(i).id) {
//					empilhado = true;
//				}
//				;
//			}
//
//			if (empilhado == false) // se ainda não foi empilhada, empilhar
//				boxEmpilhar.add(rot.get(i));
//
//			if (boxEmpilhar.size() == n) { // se a quantidade de caixas empiladas = quantidade da entrada terminou
//				System.out.println(boxEmpilhar.size() + " Caixas empilhadas.");
//				break;
//			}
//
//		}
//
//		for (caixa box : boxEmpilhar) { // mostrar caixas empilhadas em oredem decrescente
//			System.out.println("\t" + box);
//		}
//		
		

	}
	
	public boolean empilhado(ArrayList<caixa> boxEmpilhar, caixa rot) {
		for (caixa box : boxEmpilhar) { // verificar nas caixas empilhadas se a atual foi empilhada
			if (box.id == rot.id) {
				return true;
			}
		}
		return false;
	}

}
