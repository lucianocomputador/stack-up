/**
 * Problema de empilhamento de caixas em Java
 * 
 * @local IFPB - Monteiro
 * @disciplina Análise de Algoritimos
 * @author Luciano Azevedo
 * @data junho/2019
 * 
 *       Classe responsável por representação de caixa
 */

public class caixa implements Comparable<caixa> {

	/**
	 * Como representar as caixas h --> altura, w --> largura, d --> profundidade
	 */
	int id, h, w, d, area;

	/**
	 * Construtor padrão
	 */
	public caixa() {

	}

	/**
	 * Inicializar caixa sem calculo de área,ou seja criar caixas
	 * 
	 * @param id Identificador da caixa
	 * @param h  altura
	 * @param w  largura
	 * @param d  profundidade
	 */
	public caixa(int id, int h, int w, int d) {
		this.id = id;
		this.h = h;
		this.w = w;
		this.d = d;
	}

	/**
	 * Inicializar caixa com alculo de área
	 * 
	 * @param id   identificador da caixa
	 * @param h    altura
	 * @param w    largura
	 * @param d    profundidade
	 * @param area qualquer valor
	 */
	public caixa(int id, int h, int w, int d, int area) {
		this.id = id;
		this.h = h;
		this.w = w;
		this.d = d;
		this.area = w * d;
	}

	/**
	 * Para classificar a matriz de caixa com base de área em ordem decrescente de
	 * área
	 */
	@Override
	public int compareTo(caixa o) {
		return o.area - this.area;
	}

	/**
	 * Como mostrar a caixa
	 */
	@Override
	public String toString() {
		return this.id + " " + this.h + " " + this.w + " " + this.d + " " + this.area;
	}
}
